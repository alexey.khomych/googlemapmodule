//
//  GooglePlacesRouter.swift
//  AddressSearchModule
//
//  Created by Alexey Khomych on 14.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Alamofire

enum GooglePlacesRouter {
    case geocodeStreet(coordinates: LocationCoordinate)
}

extension GooglePlacesRouter: BaseRouterProtocol {
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var path: String {
        switch self {
        case .geocodeStreet:
            return "geocode/json?"
        }
    }
    
    var queryParameters: [String: String]? {
        switch self {
        case let .geocodeStreet(coordinates):
            var parameters: [String: String] = [:]
            parameters["latlng"] = "\(coordinates.latitude),\(coordinates.longitude)"
            return parameters
        }
    }
}
