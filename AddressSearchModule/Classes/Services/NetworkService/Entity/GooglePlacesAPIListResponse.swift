//
//  GooglePlacesAPIListResponse.swift
//  AddressSearchModule
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

struct GooglePlacesAPIListResponse: Codable {
    let plusCode: PlusCode
    let results: [GeocodeResult]
    let status: String
}

struct PlusCode: Codable {
    let compoundCode, globalCode: String
}

struct GeocodeResult: Codable {
    let addressComponents: [AddressComponent]
    let formattedAddress: String
    let geometry: Geometry
    let placeId: String
    let plusCode: PlusCode?
    let types: [String]
}

struct AddressComponent: Codable {
    let longName: String
    let shortName: String
    let types: [String]
}

struct Geometry: Codable {
    let location: Location
    let locationType: String
    let viewport: Bounds
    let bounds: Bounds?
}

struct Bounds: Codable {
    let northeast: Location
    let southwest: Location
}

struct Location: Codable, Equatable, Hashable {
    let lat: Double
    let lng: Double
}

extension Location {
    var stringRepresentation: String {
        return String(format: "%f, %f", lat, lng)
    }
}
