//
//  MapAddressSelectionViewModel.swift
//  AddressSearchModule
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

struct MapAddressSelectionViewModel {
    let city: String
    let formattedAddress: String
}
