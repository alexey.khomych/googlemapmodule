//
//  MapAddressSelectionInteractor.swift
//  AddressSearchModule
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

class MapAddressSelectionInteractor {

    // MARK: - Injected

    weak var output: MapAddressSelectionInteractorOutput?
    
    // MARK: - Public variables
    
    var currentCoords: Location!
    
    var locationService: LocationService!
    var placesService: PlacesService!
    
    var locationServiceStatus: LocationServiceStatus {
        return locationService.currentServiceStatus
    }
    
    // MARK: - Private Variables

    private var lastSelectedCoordinates: LocationCoordinate?
    
    private var userLocation: LocationCoordinate? {
        return locationService.currentLocation?.coordinates()
    }
}

// MARK: - Private

private extension MapAddressSelectionInteractor {
    
    enum Constants {
        static let streetName = "route"
        static let streetNumber = "street_number"
        static let locality = "locality"
        static let defaultZoomLevel: Float = 12.0
    }
    
    func getStreet(_ coordinates: LocationCoordinate) {
        lastSelectedCoordinates = coordinates
        
        placesService.geocodeStreet(coordinates: coordinates) { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let data):
                guard let place = self.configureAddressModel(data.results.first?.addressComponents) else {
                    self.addressNotFound()
                    return
                }
                self.output?.geocodeStreetFinished(with: place)
            case .failure:
                self.output?.requestFail()
            }
        }
    }
    
    func addressNotFound() {
        output?.addressNotFound()
    }
    
    func configureAddressModel(_ addressComponents: [AddressComponent]?) -> PlaceModel? {
        guard let addressComponents = addressComponents else { return nil }
        var streetNumber: String?
        var streetName: String?
        var city: String?

        addressComponents.forEach { item in
            let firstItem = item.types.first
            if firstItem == Constants.streetNumber {
                streetNumber = item.longName
            }
            
            if firstItem == Constants.streetName {
                streetName = item.shortName
            }
            
            if firstItem == Constants.locality {
                city = item.shortName
            }
        }
                
        guard let unwrappedCity = city, let unwrappedStreetNumber = streetNumber, let unwrappedStreetName = streetName,
			  let lastSelectedCoordinates = lastSelectedCoordinates else {
                return nil
        }
        
        return PlaceModel(city: unwrappedCity,
                          street: unwrappedStreetName,
                          building: unwrappedStreetNumber,
                          coordinates: lastSelectedCoordinates)
    }
}

// MARK: - MapAddressSelectionInteractorInput

extension MapAddressSelectionInteractor: MapAddressSelectionInteractorInput {
    
    func updateStreet(from location: LocationCoordinate) {
        getStreet(location)
    }
    
    func updateCamera(with place: PlaceModel) {
        output?.updateCamera(place, zoom: Constants.defaultZoomLevel)
        output?.geocodeStreetFinished(with: place)
    }
    
    func startUpdatingLocation() {
        locationService.startUpdatingLocation()
    }
}

// MARK: - LocationEventHandler

extension MapAddressSelectionInteractor: LocationEventHandler {
    
    func locationDidUpdated(_ location: UserLocation) {
        output?.cameraTargetMatchesWithUserLocation()
        output?.updateCamera(location.coordinates(), zoom: Constants.defaultZoomLevel)
        locationService.stopUpdatingLocation()
    }
    
    func updateDenied() {
        output?.locationAccessDenied()
    }
    
    func updateLocation() {
        locationService.startUpdatingLocation()
    }
}
