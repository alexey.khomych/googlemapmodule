//
//  PlaceModel.swift
//  AddressSearchModule
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

struct PlaceModel {
    let city: String
    let street: String
    let building: String?
    let coordinates: LocationCoordinate
}
