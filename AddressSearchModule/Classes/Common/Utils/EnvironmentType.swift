//
//  EnvironmentType.swift
//  GitHubRepositories
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

protocol EnvironmentType {
    var googleMapsAPIKey: String { get }
    var googlePlacesAPIKey: String { get }
    var googlePlacesAPIBaseURL: String { get }
}

struct Environment: EnvironmentType {
    let googleMapsAPIKey: String
    var googlePlacesAPIKey: String
    var googlePlacesAPIBaseURL: String
}
