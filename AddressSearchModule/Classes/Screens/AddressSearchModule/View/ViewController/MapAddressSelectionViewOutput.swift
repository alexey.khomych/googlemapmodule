//
//  MapAddressSelectionViewOutput.swift
//  AddressSearchModule
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

protocol MapAddressSelectionViewOutput: class {
    func onViewDidLoad()
    func onUserLocationPress()
    func onFindMePress()
    func onMapMove(with coordinates: LocationCoordinate)
    func onViewMapWillMove()
    func onViewMapIdleAtPosition()
    func onViewMapZoomTap()
}
