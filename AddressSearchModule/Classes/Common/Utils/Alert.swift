//
//  Alert.swift
//  GitHubRepositories
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

protocol PresentableAsAlert {
    func alertController() -> UIAlertController
}

extension UIAlertController {
    static func alertController(title: String?, message: String?, actions: [UIAlertAction], style: UIAlertController.Style = .alert) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        actions.forEach { alert.addAction($0) }
        
        return alert
    }
}

enum Alert {
    case error(message: String)
    case errorWithAction(message: String, okAction: AlertActionBlock?)
    case errorWithTwoActions(message: String, okAction: AlertActionBlock?, cancelAction: AlertActionBlock?)
    case errorWithMessage(_ message: String, actions: [UIAlertAction])
    case errorTitleAndMessage(_ title: String, _ message: String, actions: [UIAlertAction])
    case actionSheet(message: String?, actions: [UIAlertAction])
}

extension Alert: CustomStringConvertible {
    var description: String {
        switch self {
        case let .error(message),
             let .errorWithAction(message, _),
             let .errorWithTwoActions(message, _, _),
             let .errorWithMessage(message, _),
             let .errorTitleAndMessage(_, message, _):
            return message
        case let .actionSheet(message, _):
            return message ?? ""
        }
    }
}

extension Alert: PresentableAsAlert {
    func alertController() -> UIAlertController {
        return .alertController(title: title, message: description, actions: actions)
    }
    
    func actionSheet() -> UIAlertController {
        return .alertController(title: title, message: !description.isEmpty ? description : nil, actions: actions, style: .actionSheet)
    }
}

private extension Alert {
    var title: String? {
        switch self {
        case .error,
             .errorWithAction,
             .errorWithTwoActions,
             .errorWithMessage,
             .actionSheet:
            return nil
        case let .errorTitleAndMessage(title, _, _):
            return title
        }

    }

    var actions: [UIAlertAction] {
        switch self {
        case .error:
            return [ .okAction() ]
        case let .errorWithAction(_, okAction):
            return [ .okAction(okAction) ]
        case let .errorWithTwoActions(_, okAction, cancelAction):
            return [.okAction(okAction, style: .default), .cancelAction(cancelAction)]
        case let .errorWithMessage(_, actions):
            return actions
        case let .errorTitleAndMessage(_, _, actions):
            return actions
        case let .actionSheet(_, actions):
            return actions
        }
    }
}
