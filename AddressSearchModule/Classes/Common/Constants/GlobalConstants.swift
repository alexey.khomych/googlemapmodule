//
//  GlobalConstants.swift
//  GitHubRepositories
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

typealias CompletionHandler = () -> Void

struct GlobalConstants {
    static let unknownError = "Unkown error"
    static let notFoundError = "Not found"
    static let noInternetConnection = "No internet connection"
    
    static let okTitle = "Ok"
    static let cancelTitle = "Cancel"
}
