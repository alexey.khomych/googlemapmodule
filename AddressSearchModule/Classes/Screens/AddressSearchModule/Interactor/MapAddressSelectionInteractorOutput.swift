//
//  MapAddressSelectionInteractorOutput.swift
//  AddressSearchModule
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

protocol MapAddressSelectionInteractorOutput: class {
	func geocodeStreetFinished(with model: PlaceModel)
    
    func updateCamera(_ coordinates: LocationCoordinate, zoom: Float)
    func updateCamera(_ placeModel: PlaceModel, zoom: Float)
    
    func addressNotFound()
    func locationAccessDenied()
    
    func cameraTargetMatchesWithUserLocation()
    func requestFail() 
}
