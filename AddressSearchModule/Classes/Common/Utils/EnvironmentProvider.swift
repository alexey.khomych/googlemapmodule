//
//  EnvironmentProvider.swift
//  GitHubRepositories
//
//  Created by Alexey Khomych on 26.01.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

protocol EnvironmentProvider {
    func loadEnviroment() -> EnvironmentType
}

private struct EnvironmentKeys {
    static let googleMapsApiKey = "GoogleMapsApiKey"
    static let googlePlacesApiKey = "GooglePlacesAPIKey"
    static let googlePlacesApiBaseUrl = "GooglePlacesAPIBaseURL"
}

class EnvironmentProviderImpl: EnvironmentProvider {
    
    func loadEnviroment() -> EnvironmentType {
        let bundle = Bundle(for: type(of: self))

        guard let infoDict = bundle.infoDictionary else {
            fatalError("No info plist configuration in bundle!")
        }
        
        guard let googleMapsAPIKey = infoDict[EnvironmentKeys.googleMapsApiKey] as? String else {
            fatalError("Info.plist has no googleMapsAPIKey for needed configuration!")
        }
        
        guard let googlePlacesAPIKey = infoDict[EnvironmentKeys.googlePlacesApiKey] as? String else {
            fatalError("Info.plist has no googlePlacesAPIKey for needed configuration!")
        }
        
        guard let googlePlacesAPIBaseURL = infoDict[EnvironmentKeys.googlePlacesApiBaseUrl] as? String else {
            fatalError("Info.plist has no googlePlacesAPIBaseURL for needed configuration!")
        }

        return Environment(googleMapsAPIKey: googleMapsAPIKey,
                           googlePlacesAPIKey: googlePlacesAPIKey,
                           googlePlacesAPIBaseURL: googlePlacesAPIBaseURL)
    }
}
