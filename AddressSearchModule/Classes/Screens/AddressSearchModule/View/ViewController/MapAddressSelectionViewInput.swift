//
//  MapAddressSelectionViewInput.swift
//  AddressSearchModule
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

protocol MapAddressSelectionViewInput: BaseViewProtocol {
    var state: MapInteractionState { get set }
    
    func updateStreetName(with model: MapAddressSelectionViewModel)
    
    func updateCamera(_ coordinates: LocationCoordinate, zoom: Float)
    func updateStreetAndCamera(with model: MapAddressSelectionViewModel, coordinates: LocationCoordinate, zoom: Float)
    func addressNotFound()
}
