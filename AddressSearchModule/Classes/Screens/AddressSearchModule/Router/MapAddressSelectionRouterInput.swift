//
//  MapAddressSelectionRouterInput.swift
//  AddressSearchModule
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

protocol MapAddressSelectionRouterInput: class {
    func openLocationSettings()
}
