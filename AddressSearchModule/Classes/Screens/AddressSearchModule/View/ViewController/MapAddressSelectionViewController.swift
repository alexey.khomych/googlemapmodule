//
//  MapAddressSelectionViewController.swift
//  AddressSearchModule
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit
import GoogleMaps

class MapAddressSelectionViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet private weak var bottomViewContainer: UIView!
    @IBOutlet private weak var findMeButton: UIButton!
    
    @IBOutlet private weak var cityLabelBackgroundView: LabelWhiteBackground!
    @IBOutlet private weak var streetLabelBackgroundView: LabelWhiteBackground!
    @IBOutlet private weak var pinImageView: UIImageView!
    
    @IBOutlet private weak var locationButton: UIButton!
    
    @IBOutlet private weak var mapManager: MapViewManager!
    
    @IBOutlet private weak var bottomButtonsHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var bottomLocationButtonContainerConstraint: NSLayoutConstraint!

    
    // MARK: - Injected

    var output: MapAddressSelectionViewOutput!
    
    // MARK: - Properties
    
    var state: MapInteractionState = .default {
        didSet {
            switch state {
            case .default, .mapZooming, .focus:
                break
            case .mapMoving:
                streetLabelBackgroundView.state = .grey
            
                updateBottomButtonsHeightConstraint(0)
                updateBottomLocationButtonConstraint(bottomButtonsHeightConstraint.constant)
                bottomViewContainer.isHidden = true
                didUserMoveCamera = true
                locationButton.isHidden = false
                break
            case .ready:
                updateBottomButtonsHeightConstraint(Constants.bottomButtonsContainerHeight)
                updateBottomLocationButtonConstraint(Constants.bottomButtonsContainerHeight + Constants.spacingBetweenBottomViewAndLocationButton)
                bottomViewContainer.isHidden = false
                findMeButton.isHidden = true
                locationButton.isHidden = false
            }
        }
    }

    private var didUserMoveCamera = false

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        output.onViewDidLoad()
    }
}

// MARK: - Private

private extension MapAddressSelectionViewController {
    
    // MARK: - Action
    
    @IBAction func onFindMeButtonPress() {
        output.onFindMePress()
    }
    
    @IBAction func onLocationButtonPress() {
        output.onUserLocationPress()
    }
    
    enum Constants {
        static let zoomControlToCenterYDefaultConstraint: CGFloat = 50
        static let zoomControlToToastMinSpacing: CGFloat = 10
        static let bottomButtonsContainerHeight: CGFloat = 48.0
        static let spacingBetweenBottomViewAndLocationButton: CGFloat = 15.0
        static let delay = 0.3
    }
    
    func setupUI() {
        locationButton.isHidden = true
        
        findMeButton.adjustsImageWhenHighlighted = false
        locationButton.adjustsImageWhenHighlighted = false
        
        updateBottomLocationButtonConstraint(bottomButtonsHeightConstraint.constant)
        
        streetLabelBackgroundView.state = .hidden
    }
    
    func updateBottomLocationButtonConstraint(_ value: CGFloat) {
        updateConstraint(bottomLocationButtonContainerConstraint, with: value)
    }
    
    func updateBottomButtonsHeightConstraint(_ value: CGFloat) {
        updateConstraint(bottomButtonsHeightConstraint, with: value)
    }
    
    func updateConstraint(_ constraint: NSLayoutConstraint, with value: CGFloat) {
        UIView.animate(withDuration: Constants.delay) {
            constraint.constant = value
            self.view.updateConstraintsIfNeeded()
        }
    }
}

// MARK: - MapAddressSelectionViewInput

extension MapAddressSelectionViewController: MapAddressSelectionViewInput {
    
    func updateStreetName(with model: MapAddressSelectionViewModel) {
        let address = model.formattedAddress
        
        streetLabelBackgroundView.state = .update(text: address)
        cityLabelBackgroundView.state = .update(text: model.city)
    }
    
    func updateCamera(_ coordinates: LocationCoordinate, zoom: Float) {
        mapManager.moveCamera(to: coordinates, zoom: zoom)
    }
    
    func updateStreetAndCamera(with model: MapAddressSelectionViewModel, coordinates: LocationCoordinate, zoom: Float) {
        let address = model.formattedAddress
        streetLabelBackgroundView.state = .update(text: address)
        cityLabelBackgroundView.state = .update(text: model.city)
        mapManager.moveCamera(to: coordinates, zoom: zoom)
    }
    
    func addressNotFound() {
        streetLabelBackgroundView.state = .hidden
    }
}

// MARK: - GMSMapViewDelegate

extension MapAddressSelectionViewController: GMSMapViewDelegate {
   
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if didUserMoveCamera && state != .mapZooming {
            output.onMapMove(with: mapView.camera.target)
        }
        
        if state == .focus {
            didUserMoveCamera = false
        }
        
        output.onViewMapIdleAtPosition()
    }

    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        output.onViewMapWillMove()
    }
}
