//
//  LocationService.swift
//  AddressSearchModule
//
//  Created by Alexey Khomych on 31.01.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation
import CoreLocation

struct UserLocation {
    let location: CLLocation

    func coordinates() -> CLLocationCoordinate2D {
        return location.coordinate
    }
}

protocol LocationEventHandler: class {
    func updateLocation()
    func updateDenied()
    func locationDidUpdated(_ location: UserLocation)
}

extension LocationEventHandler {
    func updateLocation() {}
    func updateDenied() {}
}

typealias LocationRefreshCompletion = ((UserLocation?) -> Void)

protocol LocationService {
    var handler: LocationEventHandler? { get set }

    var currentLocation: UserLocation? { get }
    var isLocationEnabled: Bool { get }
    
    var currentServiceStatus: LocationServiceStatus { get }

    func refreshCurrentLocation(completion: @escaping LocationRefreshCompletion)
    func startUpdatingLocation()
    func stopUpdatingLocation()
}

enum LocationServiceStatus {
    case authorized
    case denied
    case notAuthorized
}

final class LocationServiceImpl: NSObject, LocationService {

    // MARK: - Public Variables

    weak var handler: LocationEventHandler?
    var isLocationEnabled: Bool {
        return checkCurrentStatus() == .authorized
    }
    
    var currentServiceStatus: LocationServiceStatus {
        return checkCurrentStatus()
    }

    var currentLocation: UserLocation? {
        guard let location = locationManager.location else {
            return nil
        }
        return UserLocation(location: location)
    }

    // MARK: - Private Variables

    private let locationManager: CLLocationManager = CLLocationManager()
    private var isListeningForPermissionChange = false
    private var currentStatus: LocationServiceStatus = .notAuthorized
    private var refreshCompletion: LocationRefreshCompletion?

    // MARK: - Init

    override init() {
        super.init()

        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
    }

    // MARK: - Private

    private func checkCurrentStatus() -> LocationServiceStatus {
        if (CLLocationManager.authorizationStatus() == .notDetermined) {
            return .notAuthorized
        } else if (CLLocationManager.authorizationStatus() == .denied) {
            return .denied
        } else {
            return .authorized
        }
    }

    private func checkLocationServiceWithAction(_ action: () -> Void) {
        isListeningForPermissionChange = true
        switch checkCurrentStatus() {
        case .authorized:
            action()
        case .denied:
            handler?.updateDenied()
        case .notAuthorized:
            locationManager.requestAlwaysAuthorization()
        }
    }

    // MARK: - Public

    func refreshCurrentLocation(completion: @escaping LocationRefreshCompletion) {
        locationManager.stopUpdatingLocation()

        if !isLocationEnabled {
            completion(nil)
            return
        }

        refreshCompletion = completion
        locationManager.requestLocation()
    }

    func startUpdatingLocation() {
        checkLocationServiceWithAction {
            locationManager.startUpdatingLocation()
        }
    }

    func stopUpdatingLocation() {
        locationManager.stopUpdatingLocation()
    }
}

extension LocationServiceImpl: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        var userLocation: UserLocation?
        if let location = locations.last {
            userLocation = UserLocation(location: location)
            handler?.locationDidUpdated(userLocation!)
        } else {
            handler?.updateDenied()
        }

        refreshCompletion?(userLocation)
        refreshCompletion = nil
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if isListeningForPermissionChange {
            isListeningForPermissionChange = false
            if status == .denied {
                handler?.updateDenied()
            } else if status == .authorizedAlways || status == .authorizedWhenInUse {
                handler?.updateLocation()
            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        refreshCompletion?(nil)
        refreshCompletion = nil
    }
}
