//
//  MapViewManager.swift
//  AddressSearchModule
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import GoogleMaps

class MapViewManager: NSObject {
    
    // MARK: - Outlets
    
    @IBOutlet private weak var mapView: GMSMapView!
    
    // MARK: - Public
    
    func zoomIn() {
        mapView.animate(toZoom: mapView.camera.zoom + 1)
    }
    
    func zoomOut() {
        mapView.animate(toZoom: mapView.camera.zoom - 1)
    }
}

// MARK: - Private

private extension MapViewManager {
    
    enum Constants {
        static let defaultCameraPadding: CGFloat = 90
        static let defaultSelectedMarkerCameraZoom: Float = 13
        static let defaultUserLocationCameraZoom: Float = 11
        static let defaultSinglePinZoomLevel: Float = 13.0
    }
}

// MARK: - MapLocationService

extension MapViewManager: MapLocationService {
    
    func mapZoomLevel() -> Float {
        return mapView.camera.zoom
    }
    
    func moveCamera(to coordinate: LocationCoordinate, zoom: Float = Constants.defaultUserLocationCameraZoom) {
        mapView.moveCamera(GMSCameraUpdate.setTarget(coordinate, zoom: zoom))
    }
    
    func setupZoomLevel(_ minZoom: Float, maxZoom: Float) {
        mapView.setMinZoom(minZoom, maxZoom: maxZoom)
    }
}
