//
//  MapAddressSelectionViewModelBuilder.swift
//  AddressSearchModule
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

protocol MapAddressSelectionViewModelBuilder {
    func build(with model: PlaceModel) -> MapAddressSelectionViewModel
}

class MapAddressSelectionViewModelBuilderImpl: MapAddressSelectionViewModelBuilder {
    func build(with model: PlaceModel) -> MapAddressSelectionViewModel {
        return MapAddressSelectionViewModel(city: model.city,
                                            formattedAddress: String(format: "%@, %@", model.street, model.building ?? ""))
    }
}
