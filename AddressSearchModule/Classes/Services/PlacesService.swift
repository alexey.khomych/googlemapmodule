//
//  PlacesService.swift
//  AddressSearchModule
//
//  Created by Alexey Khomych on 31.01.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation
import Alamofire

typealias NetworkRequestCompletion<T: Decodable, E: Error> = (Swift.Result<T, APIError>) -> Void

struct APIError: Error {
    let errorDescription: String
}

enum PlaceParserError: Error {
    case googleError(String)
    case addressNotFound
}

protocol PlacesService {
    func geocodeStreet(coordinates: LocationCoordinate,
                       completion: @escaping NetworkRequestCompletion<GooglePlacesAPIListResponse, APIError>)
}

final class PlacesServiceImpl: PlacesService {

    // MARK: - Public

    func geocodeStreet(coordinates: LocationCoordinate,
                       completion: @escaping NetworkRequestCompletion<GooglePlacesAPIListResponse, APIError>) {

        var storedError: APIError?
        let router = GooglePlacesRouter.geocodeStreet(coordinates: coordinates)

        _ = Alamofire.request(router).responseJSON { (dataResponse) in
            switch dataResponse.result {
            case .success:
                if let data = dataResponse.data {
                    do {
                        let decoder = JSONDecoder.default
                        let response = try decoder.decode(GooglePlacesAPIListResponse.self, from: data)
                        completion(.success(response))
                    } catch {
                        storedError = APIError(errorDescription: "Corruped response")
                    }
                }
            case let .failure(error):
                storedError = APIError(errorDescription: error.localizedDescription)
            }
            
            if let storedError = storedError {
                completion(.failure(storedError))
            }
        }
    }
}
