//
//  MapAddressSelectionInteractorInput.swift
//  AddressSearchModule
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

protocol MapAddressSelectionInteractorInput: class {
    
    var locationServiceStatus: LocationServiceStatus { get }
    
    func updateStreet(from location: LocationCoordinate)
    func updateCamera(with place: PlaceModel)
    
    func startUpdatingLocation()
}
