//
//  JSONDecoder+Default.swift
//  AddressSearchModule
//
//  Created by Alexey Khomych on 14.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

extension JSONDecoder {
    static var `default`: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()
}
