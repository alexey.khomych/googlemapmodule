//
//  MapAddressSelectionPresenter.swift
//  AddressSearchModule
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit
import CoreLocation

enum MapInteractionState {
    case `default`
    case mapMoving
    case mapZooming
    case ready
    case focus
}

class MapAddressSelectionPresenter {

    // MARK: - Injected

    weak var view: MapAddressSelectionViewInput?
    var interactor: MapAddressSelectionInteractorInput!
    var router: MapAddressSelectionRouterInput!
    var mapAddressBuilder: MapAddressSelectionViewModelBuilder!
}

// MARK: - MapAddressSelectionModuleInput

extension MapAddressSelectionPresenter: MapAddressSelectionModuleInput {
    
}

// MARK: - Private

private extension MapAddressSelectionPresenter {
    
    enum Constants {
        static let cancelTitle = "Cancel"
        static let okTitle = "Ok"
        static let settingsTitle = "Settings"
        static let alertLocationMessageAccess = "Нужен доступ к геолокации"
        static let defaultLatCoord: CLLocationDegrees = 50.45466
        static let defaultLongCoord: CLLocationDegrees = 30.5238
        static let defaultZoomLevel: Float = 15.0
    }
}

// MARK: - MapAddressSelectionViewOutput

extension MapAddressSelectionPresenter: MapAddressSelectionViewOutput {

    func onViewDidLoad() {
        view?.state = .default
        let defaultLocation = LocationCoordinate(latitude: Constants.defaultLatCoord, longitude: Constants.defaultLongCoord)
        view?.updateCamera(defaultLocation, zoom: Constants.defaultZoomLevel)
    }
    
    func onFindMePress() {
        interactor.startUpdatingLocation()
    }
    
    func onUserLocationPress() {
        interactor.startUpdatingLocation()
    }
    
    func onMapMove(with coordinates: LocationCoordinate) {
        interactor.updateStreet(from: coordinates)
    }
    
    func onViewMapWillMove() {
        if view?.state != .mapZooming {
            view?.state = .mapMoving
        }
    }
    
    func onViewMapIdleAtPosition() {
        view?.state = .default
    }
    
    func onViewMapZoomTap() {
        if view?.state != .mapMoving {
            view?.state = .mapZooming
        }
    }
}

// MARK: - MapAddressSelectionInteractorOutput

extension MapAddressSelectionPresenter: MapAddressSelectionInteractorOutput {
        
	func geocodeStreetFinished(with model: PlaceModel) {
		view?.updateStreetName(with: mapAddressBuilder.build(with: model))
    }
    
    func updateCamera(_ coordinates: LocationCoordinate, zoom: Float) {
        view?.updateCamera(coordinates, zoom: zoom)
    }
    
    func updateCamera(_ placeModel: PlaceModel, zoom: Float) {
        view?.updateStreetAndCamera(with: mapAddressBuilder.build(with: placeModel), coordinates: placeModel.coordinates, zoom: zoom)
    }
    
    func locationAccessDenied() {
        let settingsAction = UIAlertAction(title: Constants.settingsTitle, style: .cancel) { _ in
            self.router.openLocationSettings()
        }
        
        let notNowAction = UIAlertAction(title: Constants.cancelTitle, style: .default) { _ in

        }
        
        view?.presentAlert(.errorWithMessage(Constants.alertLocationMessageAccess, actions: [notNowAction, settingsAction]))
    }
    
    func addressNotFound() {
        view?.addressNotFound()
        view?.state = .default
    }
    
    func cameraTargetMatchesWithUserLocation() {
        view?.state = .focus
    }
    
    func requestFail() {
        
    }
}
