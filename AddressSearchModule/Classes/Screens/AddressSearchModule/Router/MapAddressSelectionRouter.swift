//
//  MapAddressSelectionRouter.swift
//  AddressSearchModule
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

class MapAddressSelectionRouter {
    
    // MARK: - Injected
    
    weak var view: MapAddressSelectionViewInput?
}

// MARK: MapAddressSelectionRouterInput

extension MapAddressSelectionRouter: MapAddressSelectionRouterInput {
    
    func openLocationSettings() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl)
        }
    }
}
