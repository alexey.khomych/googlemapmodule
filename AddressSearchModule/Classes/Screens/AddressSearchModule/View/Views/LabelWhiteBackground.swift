//
//  LabelWhiteBackground.swift
//  AddressSearchModule
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

class LabelWhiteBackground: NibView {
    
    // MARK: - Outlets
    
    @IBOutlet weak var textLabel: UILabel!
    
    // MARK: - Public Variable
    
    enum State {
        case `default`
        case hidden
        case grey
        case update(text: String)
    }
    
    var state: State = .default {
        didSet {
            switch state {
            case .default:
                isHidden = false
                textLabel.textColor = .black
            case .hidden:
                isHidden = true
            case .grey:
                isHidden = false
                textLabel.textColor = .gray
                textLabel.text = Constants.mapPinPlaceholder
            case .update(let text):
                isHidden = false
                textLabel.textColor = .black
                textLabel.text = text
            }
        }
    }
}

private extension LabelWhiteBackground {
    
    enum Constants {
        static let mapPinPlaceholder = "Поиск адреса..."
    }
}
