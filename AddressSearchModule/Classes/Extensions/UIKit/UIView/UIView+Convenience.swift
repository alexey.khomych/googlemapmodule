//
//  UIView+Convenience.swift
//  GitHubRepositories
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

// MARK: - Nib

extension UIView {
    
    static var nib: UINib {
        let bundle = Bundle(for: self as AnyClass)
        let nib = UINib(nibName: String(describing: self), bundle: bundle)
        return nib
    }

    static func loadFromNib() -> Self {
        return _loadFromNib()
    }

    // MARK: - Private

    fileprivate static func _loadFromNib<T: UIView>() -> T {
        guard let view = nib.instantiate(withOwner: nil, options: nil).first as? T else {
            fatalError("The nib \(nib) expected its root view to be of type \(self)")
        }
        return view
    }
}
