//
//  ReusableView.swift
//  GitHubRepositories
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

protocol ReusableView {
    static func reuseIdentifier() -> String
}

extension ReusableView {
    
    static func reuseIdentifier() -> String {
        return String(describing: self)
    }
}

// MARK: - UITableViewCell

extension ReusableView where Self: UITableViewCell {
    
    static func registerFor(tableView: UITableView) {
        tableView.register(nib, forCellReuseIdentifier: Self.reuseIdentifier())
    }
}

extension UITableViewCell: ReusableView {}

// MARK: - UICollectionViewCell

extension ReusableView where Self: UICollectionViewCell {
    
    static func registerFor(collectionView: UICollectionView) {
        collectionView.register(nib, forCellWithReuseIdentifier: Self.reuseIdentifier())
    }
}

extension UICollectionViewCell: ReusableView {}

// MARK: - UITableViewHeaderFooterView

extension UITableViewHeaderFooterView: ReusableView {}

extension ReusableView where Self: UITableViewHeaderFooterView {
    
    static func registerFor(tableView: UITableView) {
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: Self.reuseIdentifier())
    }
}
