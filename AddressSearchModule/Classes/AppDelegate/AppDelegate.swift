//
//  AppDelegate.swift
//  GitHubRepositories
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let assembly = MapAddressSelectionAssembly()
        assembly.assemble()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = assembly.viewController!
        window?.makeKeyAndVisible()
        
        return true
    }
}
