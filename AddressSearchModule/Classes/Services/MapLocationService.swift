//
//  MapLocationService.swift
//  AddressSearchModule
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation
import GoogleMaps

typealias CameraPosition = GMSCameraPosition
typealias LocationCoordinate = CLLocationCoordinate2D

protocol MapLocationService {
    func mapZoomLevel() -> Float
    func moveCamera(to coordinate: LocationCoordinate, zoom: Float)
    func setupZoomLevel(_ minZoom: Float, maxZoom: Float)
}
