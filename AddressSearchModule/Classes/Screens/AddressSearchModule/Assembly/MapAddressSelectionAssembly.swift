//
//  MapAddressSelectionAssembly.swift
//  AddressSearchModule
//
//  Created by Alexey Khomych on 10.02.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps

class MapAddressSelectionAssembly {
    
    var interactor: MapAddressSelectionInteractor?
    var router: MapAddressSelectionRouter?
    var presenter: MapAddressSelectionPresenter?
    var viewController: MapAddressSelectionViewController?
}

// MARK: - Private

private extension MapAddressSelectionAssembly {
    
    func prepareInteractor() {
        let interactor = MapAddressSelectionInteractor()
        let locationService = LocationServiceImpl()
        locationService.handler = interactor
        interactor.locationService = locationService
        interactor.placesService = PlacesServiceImpl()
        self.interactor = interactor
    }
    
    func preparePresenter() {
        let presenter = MapAddressSelectionPresenter()
        presenter.view = viewController
        
        interactor?.output = presenter
        presenter.interactor = interactor
        
        let router = MapAddressSelectionRouter()
        router.view = viewController
        presenter.router = router
        
        presenter.mapAddressBuilder = MapAddressSelectionViewModelBuilderImpl()
        viewController?.output = presenter
        self.presenter = presenter
    }
    
    func prepareRouter() {
        let router = MapAddressSelectionRouter()
        router.view = viewController
        self.router = router
    }
    
    func prepareViewController() {
        let viewControllerName = "MapAddressSelectionViewController"
        let storyBoard = UIStoryboard(name: viewControllerName, bundle: nil)
        let id = String(describing: viewControllerName)
        let controller = storyBoard.instantiateViewController(withIdentifier: id)
        
        if let viewController = controller as? MapAddressSelectionViewController {
            self.viewController = viewController
        }
    }
    
    func prepareGoogleService() {
        let enviroment = EnvironmentProviderImpl().loadEnviroment()
        let mapsAPIKey = enviroment.googleMapsAPIKey
        let placesAPIKey = enviroment.googlePlacesAPIKey
        GMSServices.provideAPIKey(mapsAPIKey)
        GMSPlacesClient.provideAPIKey(placesAPIKey)
    }
}

// MARK: - Assembly

extension MapAddressSelectionAssembly {

    func assemble() {
        prepareGoogleService()
        prepareViewController()
        prepareInteractor()
        preparePresenter()
        prepareRouter()
    }
}
